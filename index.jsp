<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>视频预览</title>
  <!--引入CSS文件-->
  <link rel="stylesheet" href="lib/js/easyui/easyui.css">
  <link rel="stylesheet" href="lib/css/bootstrap.css">
  <link rel="stylesheet" href="lib/css/bootstrap-treeview.css">
  <link rel="stylesheet" href="lib/css/buildpage.css">
</head>
<body>
</body>
<script type="text/javascript" src="lib/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="lib/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="lib/js/bootstrap-treeview.js"></script>
<script type="text/javascript" src="lib/js/webVideoCtrl.js"></script>
<script type="text/javascript" src="lib/js/demo.js"></script>
<script type="text/javascript" src="view/video.js"></script>
</html>
